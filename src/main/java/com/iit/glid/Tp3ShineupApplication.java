package com.iit.glid;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Tp3ShineupApplication {

	public static void main(String[] args) {
		SpringApplication.run(Tp3ShineupApplication.class, args);
	}

}
