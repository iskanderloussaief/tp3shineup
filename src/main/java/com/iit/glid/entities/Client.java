package com.iit.glid.entities;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Client {

	@Id
	private String code;
	private String raison_sociale;
	private String adresse;
	private String telephone;

	public Client() {

	}
	
	
	public Client(String raison_sociale, String adresse, String telephone) {
		this.raison_sociale = raison_sociale;
		this.adresse = adresse;
		this.telephone = telephone;
	}

	public Client(String code, String raison_sociale, String adresse, String telephone) {
		this.code = code;
		this.raison_sociale = raison_sociale;
		this.adresse = adresse;
		this.telephone = telephone;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getRaison_sociale() {
		return raison_sociale;
	}

	public void setRaison_sociale(String raison_sociale) {
		this.raison_sociale = raison_sociale;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

}
